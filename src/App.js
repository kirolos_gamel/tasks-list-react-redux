import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const Layout = React.lazy(() => import('./containers/Layout'));

// Pages
const Login = React.lazy(() => import('./views/Login/Login'));


class App extends Component {
  state = {
    loggedIn: false,
  };

  componentDidMount(){
    const loginStatus = sessionStorage.getItem('loginStatus');
    if(loginStatus){
      this.setState({
        loggedIn: true
      });
    }
    else{
      this.setState({
        loggedIn: false
      });
    }
  }

  /**
   * Route system here there are inner protected routes inside Layout, you should login to view them,
   * route.js file contains these protected routes
   * Routes outside inner routes are not protected like login page
   */
  render() {

    return (
      <Router>
        <div>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route exact path="/login" name="Login Page"  render= {props=> { return !this.state.loggedIn ? <Login {...props}/> : <Redirect to="/tasks" />;  } }  />
              <Route path="/" name="Home" render={props => { return this.state.loggedIn ? <Layout {...props}/> : <Redirect to="/login" />; } } />
            </Switch>
            
          </React.Suspense>
          </div>
      </Router>
    );
  }
}

export default App;

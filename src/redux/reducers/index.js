import { combineReducers } from 'redux';

import {loginSuccess, loginRequest, loginFailure} from './Login/loginReducer';

import {  fetchCategories,  tasksReducer} from './Tasks/tasksReducer';

const rootReducer = combineReducers({
    loginSuccess: loginSuccess,
    loginRequest: loginRequest,
    loginFailure: loginFailure,
    categories: fetchCategories,
    tasks: tasksReducer
});

export default rootReducer
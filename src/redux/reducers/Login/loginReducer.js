import {USERS_LOGIN_FAILURE, USERS_LOGIN_REQUEST, USERS_LOGIN_SUCCESS} from '../../actions/Login/actionTypes';

export const loginFailure = (state = false , action) =>{
    switch(action.type){
        case USERS_LOGIN_FAILURE:
         return action.payLoad;
         break;
        default:
         return state;    
         break;
     }
}

export const loginRequest = (state = false, action) =>{
    switch(action.type){
  
        case  USERS_LOGIN_REQUEST:
         return action.payLoad;
         break;
        default:
         return state;    
         break;
     }
}

export const loginSuccess = (state = false, action) =>{
    switch(action.type){
        case  USERS_LOGIN_SUCCESS:
         return  action.payLoad;
        break;
        default:
         return state;    
         break;
     }
}
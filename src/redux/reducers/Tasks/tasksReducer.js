import {ADD_NEW_TASK, DELETE_TASK, FETCH_TASKS_LIST, FILTER_TASKS_LIST, FETCH_CATEGORIES } from '../../actions/Tasks/actionTypes';

export const fetchCategories = (state = [], action) =>{
    switch(action.type){
        case FETCH_CATEGORIES:
         return action.payLoad;
         break;
        default:
         return state;    
         break;
     }
}


export const tasksReducer = (state = [], action) =>{ 
    console.log(action);
    switch(action.type){

        case FETCH_TASKS_LIST:
         return action.payLoad;
        break;

        case FILTER_TASKS_LIST:
         return action.payLoad;
        break;

        case ADD_NEW_TASK:
            return [...state, action.payLoad];
        break; 

        case DELETE_TASK:
         return  [...state.filter(item => item.id !== action.payLoad)];
        break;

        default:
         return state;    
         break;
     }
}
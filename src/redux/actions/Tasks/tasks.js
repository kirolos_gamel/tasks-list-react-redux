import {ADD_NEW_TASK, DELETE_TASK, FETCH_TASKS_LIST, FILTER_TASKS_LIST, FETCH_CATEGORIES } from './actionTypes';
import {store} from "../../store/store";
import axios from 'axios';

export const addNewTask = obj => {
    return {
        type: ADD_NEW_TASK,
        payLoad: obj
    }
};

export const deleteTask = id => {
    return {
        type: DELETE_TASK,
        payLoad: id
    }
};

export const fetchTasks = tasks => {
    return {
        type: FETCH_TASKS_LIST,
        payLoad: tasks
    }
};

export const filterTasks = data =>{
    return {
        type: FILTER_TASKS_LIST,
        payLoad: data
    }
}

export const fetchCategories = categories => {
    return {
        type: FETCH_CATEGORIES,
        payLoad: categories
    }
};

//dispatch fetchCategories
export const getCategories = () =>{
    return async (dispatch) =>{

        let categories = store.getState().categories;

        // if categories exist in store, return it
        if(categories.length) return dispatch(fetchCategories(categories));

        try{
            let categories = await axios.get('../data.json');
            return dispatch(fetchCategories(categories.data))
        }
        catch(error){
            throw error; 
        }

    }
}

//dispatch fetchTasks
export const getTasks = () =>{
    return async (dispatch) =>{
        
       let tasks = store.getState().tasks;

        // if tasks exist in store, return it
        if(tasks && tasks.length) return dispatch(fetchTasks(tasks));

        tasks = JSON.parse(sessionStorage.getItem("tasks"));

       return (tasks && tasks.length)?  dispatch(fetchTasks(tasks)) : dispatch(fetchTasks([]));


    }
}

/**
 * I got all tasks from session storage
 * filter them and then return filter list on tasks inside global store
 */
export const filterItems = (data) =>{
    return async (dispatch) =>{
        console.log(data);  
       if(!data || data.length ===0 ){
           console.log("!data || data.length ===0")
        let tasks = JSON.parse(sessionStorage.getItem("tasks"));
        return dispatch(filterTasks(tasks));
       }
       
       let tasks = JSON.parse(sessionStorage.getItem("tasks"));
       if(tasks && tasks.length > 0){
        console.log("tasks && tasks.length > 0")
        let tasksStore = JSON.parse(sessionStorage.getItem("tasks"));

        tasksStore = [...tasksStore.filter(item =>{
            for(let i=0; i< data.length; i++){
                if(item.category == data[i]){
                    return item;
                }
            }
            
        } )];
         return dispatch(filterTasks(tasksStore));

       }
       else{
        console.log("else")
           return dispatch(filterTasks([]));
       }
       
    }
}

export const addTask = (obj) =>{
    return async (dispatch) =>{
       if(!obj) return;
        console.log(obj)
        let tasks = JSON.parse(sessionStorage.getItem("tasks"));
        tasks = (tasks && tasks.length) ? tasks : [];
        console.log(tasks);

       tasks = [...tasks,obj];
        console.log( tasks );
        sessionStorage.setItem("tasks", JSON.stringify(tasks));
        return dispatch(addNewTask(obj));
    }
}

export const deleteItem = (data) =>{
    return async (dispatch) =>{
       if(!data) return;
        console.log(data);
        let tasks = JSON.parse(sessionStorage.getItem("tasks"));
        console.log(tasks);

        tasks = [...tasks.filter(item => item.id !== data)];

        console.log( tasks );
        sessionStorage.setItem("tasks", JSON.stringify(tasks));
        return dispatch(deleteTask(data));
    }
}
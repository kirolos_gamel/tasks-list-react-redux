import {USERS_LOGIN_REQUEST, USERS_LOGIN_FAILURE, USERS_LOGIN_SUCCESS} from './actionTypes';

export const loginRequest = bool =>{
    return{
        type: USERS_LOGIN_REQUEST,
        payLoad: bool
    }
}

export const loginFailure = bool =>{
    return{
        type: USERS_LOGIN_FAILURE,
        payLoad: bool
    }
}

export const loginSuccess = bool =>{
    return{
        type: USERS_LOGIN_SUCCESS,
        payLoad: bool
    }
}

/**
 * Login process dispatch all actions above and after success login will store user information in session storage
 */
export const loginProcess = (username,password) =>{
    return async (dispatch) =>{
        if(!username && !password) return;
        dispatch(loginRequest(true));
        if(username === "admin" && password === "54321"){
            sessionStorage.setItem('loginStatus', true);
            sessionStorage.setItem('userInfo', JSON.stringify({name: "Kirolos Gamel", age: "27", role: "Admin", brief: "Front-end Web Developer, Many years of experience in the career." }) );
            dispatch(loginRequest(false));
            return dispatch(loginSuccess(true))
        }
        else{
            dispatch(loginRequest(false));
           return dispatch(loginFailure(true));
        }
        dispatch(loginRequest(false));
    }
}
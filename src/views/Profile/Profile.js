import React, { Component } from 'react';

import person from '../../assets/images/person.png';

class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userInfo: {}
    }
    
  }

  componentDidMount(){
    //get userinfo from session storage, I store user info in session storage after success login
    let user = JSON.parse(sessionStorage.getItem("userInfo"));
    console.log(user);
    this.setState({
      userInfo: {...user}
    })
  }

  render() {

    return (
      
     
      <div className="container">
        <div className="row">
            <div className="col-lg-12 posts-list">
                <div className="single-post row">
                    <div className="col-lg-12">
                        <div className="feature-img text-center">
                            <img className="img-fluid" src={person} alt="" />
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 blog_details text-center">
                        <h2>{this.state.userInfo.name}</h2>
                        <p className="excert">
                            Age: {this.state.userInfo.age}
                        </p>
                        <p className="excert">
                            Role: {this.state.userInfo.role}
                        </p>
                        <p className="excert">
                           Brief: {this.state.userInfo.brief}
                        </p>
                       
                    </div>

                </div>
                
            </div>

        </div>
      </div>


    );
  }
}

export default Profile;

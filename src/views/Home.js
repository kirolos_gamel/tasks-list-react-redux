import React from 'react';
import home from '../assets/images/home.jpg';

const Home = () =>{
  return(
    <div className="container">
        <div className="row">
            <div className="col-lg-12 posts-list">
                <div className="single-post row">
                    <div className="col-lg-12">
                        <div className="feature-img text-center">
                            <img className="img-fluid" src={home} alt="" />
                        </div>
                    </div>

                    <div className="col-lg-12 col-md-12 blog_details text-center">
                        <h2>Todo List React App</h2>
                        <p className="excert">
                         Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                    </div>

                </div>
                
            </div>

        </div>
      </div>
  );
}


export default Home;

import React, { Component } from 'react';
import './login.css';
import { connect } from 'react-redux';
import {loginProcess} from '../../redux/actions/Login/login';
import {store} from "../../redux/store/store";
import CustomToast from '../../shared/Utilities/CustomToast';
class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: "",
      password: "",
      showToast: false, 
      toastMsg: '',
      toastClassName: '',
    };
    console.log(this.props);
  }
  //Function to hideToast 
  hideToast = () => {
    this.setState({ ...this.state, showToast: false, toastMsg: '', toastClassName: `bg-success` });
  }
  
  handleChange = event =>{
     this.setState({
       [event.target.name]: event.target.value
     });
   }
//submit Login form
   handleSubmit =  event =>{

    event.preventDefault();
    const btn = document.querySelector("#login-btn"); 
    btn.disabled = true;

    const {name,password} = this.state;
     console.log({name,password});
    //action creator from map dispatch to props 
    this.props.loginProcess(name,password);
      
    btn.disabled = false;
    
    console.log(store.getState());

    if(store.getState().loginSuccess === true){
      this.setState({
        showToast: true,
        toastMsg: 'Login is Successfully',
        toastClassName: `bg-success` 
      });
     // return <Redirect to={{pathname: "/profile" }} />;
     window.location.href = "/profile";
    }
    else if(store.getState().loginFailure === true){
      this.setState({
        showToast: true,
        toastMsg: 'Username or Password is wrong',
        toastClassName: `bg-danger` 
      });
    }
  }

  render() {
    const {showToast,toastMsg,toastClassName} = this.state;
    
    return (
      <>
      <div className="text-center container-login">
        <form className="form-signin" onSubmit={this.handleSubmit}>
        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label htmlFor="inputEmail" className="sr-only">User Name</label>
        <input type="text" id="inputEmail" className="form-control" placeholder="User Name" name="name" onChange={this.handleChange} required autoFocus />
        <label htmlFor="inputPassword" className="sr-only">Password</label>
        <input type="password" id="inputPassword" className="form-control" placeholder="Password" name="password" onChange={this.handleChange} required />
 
        <button className="btn btn-lg btn-primary btn-block" id="login-btn" type="submit">Sign in</button>

      </form>
      </div>
     {/* Show Toast after Login success */}
     {showToast &&
        <CustomToast
          isOpen={showToast}
          msg={toastMsg}
          cb={() => this.hideToast()}
          className={toastClassName}
        />
      }
      </>
    );
  }
}

//reducers
const mapStateToProps = (state) =>{
  return{
    loginSuccess: state.loginSuccess,
    loginRequest: state.loginRequest,
    loginFailure: state.loginFailure
  }
}

//actions 
const mapDispatchToProps =  (dispatch) =>{
  return{
    loginProcess: (username,password) =>  dispatch(loginProcess(username,password))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

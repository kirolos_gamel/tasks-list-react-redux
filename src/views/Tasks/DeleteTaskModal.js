import React, { Component } from 'react';
import { Col, Row,  Button, Modal, ModalBody, ModalFooter } from 'reactstrap';


class DeleteTaskModal extends Component { 
 
    render(){
        //props I got from Task Component when I called the Delete Modal
        const {
            isOpen,
            toggle,
            handleDelete,
            deleteId,
            className    
        } = this.props;

        return(
            <>
            <Modal isOpen={isOpen} toggle={toggle} className={'modal-danger ' + className}>
                <ModalBody>

                    <Row form>
                        <Col md={12}>
                            <h2>Do you want to delete this Task?</h2>
                        </Col>
                    </Row>

                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={(e) => handleDelete(e, deleteId)}>Delete</Button>{' '}
                    <Button color="secondary" onClick={()=>toggle(deleteId)}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
        );
    }
}

export default DeleteTaskModal;
import React, { Component } from 'react';

import { Col, Row, Button, Modal, ModalBody, ModalHeader,Form, FormGroup, Label, Input } from 'reactstrap';
import {getCategories} from '../../redux/actions/Tasks/tasks';
import { connect } from 'react-redux';
 
class AddTaskModal extends Component { 

    componentDidMount =  async()=>{ 
        //get all categories to render them on add task modal
        const {getCategories} = this.props;
        const reqs = await getCategories();
    }

    render(){
        //props I got from Task Component when I called the Add Modal
        const {
            isOpen,
            toggle,
            handleSubmit,
            handleChange,
            categories    
        } = this.props;

        console.log(this.props);

        return(
            <>

            <Modal isOpen={isOpen} toggle={toggle} className={'modal' + this.props.className}>
                <ModalHeader toggle={toggle}> Add New Task </ModalHeader>
                <ModalBody>

                <Form onSubmit={handleSubmit} >

                    <Row form>

                        <Col md={12}>
                            <FormGroup>
                                <Label >Title</Label>
                                <Input type="text" name="title" onChange={handleChange} required />
                            </FormGroup>
                        </Col>

                        <Col md={6}>
                            <FormGroup>
                                <Label >Category</Label>
                                <Input type="select" name="category" onChange={handleChange}  required>
                                    <option> </option>
                                    {categories &&
                                      <>
                                      {categories.map(category => 
                                        <option value={category.id} key={category.id}>{category.title}</option>
                                      )}
                                      </>
                                    }
                                </Input>
                            </FormGroup>
                        </Col>

                    </Row>
                    
                    <Button color="primary" name="save" id="add-task-btn" >Save</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </Form>

                </ModalBody>

            </Modal>

            </>
        );
    }
}

//reducers
const mapStateToProps = (state) =>{
    return{
      categories: state.categories
    }
  }
  
  //actions 
  const mapDispatchToProps =  (dispatch) =>{
    return{
      getCategories: async () => await dispatch(getCategories())
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddTaskModal);
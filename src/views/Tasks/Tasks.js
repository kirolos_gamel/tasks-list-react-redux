import React, { Component } from 'react';
import { connect } from 'react-redux';
import {getCategories,getTasks,addTask,deleteItem,filterItems} from '../../redux/actions/Tasks/tasks';
import {store} from "../../redux/store/store";
import AddTaskModal from './AddTaskModal';
import DeleteTaskModal from './DeleteTaskModal';
import CustomToast from '../../shared/Utilities/CustomToast';
import { Button } from 'reactstrap';
import './Tasks.css';
import deleteIcon from '../../assets/images/delete-icon.png';

class Tasks extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
     addModal: false,
     deleteModal: false,
     showToast: false, 
     toastMsg: '',
     toastClassName: '',
     categoriesFilterList: []
    };
  }

  componentDidMount = async () => {
    //get all categories and tasks to render them
    const {getCategories,getTasks} = this.props;
    const reqs = await getCategories();
    const tasks = getTasks();
    console.log({reqs, tasks});
    console.log(store.getState())
  }

  //To Open Add Modal
  toggle = () => {

    this.setState({
      addModal: !this.state.addModal,

    });

  }

  //To Open Delete Modal
  toggleDeleteModal = (id) =>{
    this.setState({
      deleteModal: !this.state.deleteModal,
      deleteId: id
    });
  }
  
  //when check any category this function will be called to filter tasks from store
  categoryFilter = (event,id,i) =>{
    console.log({event,id,i});

    let checked = event.target.checked;
    if(checked){
     this.state.categoriesFilterList = [...this.state.categoriesFilterList, id];

      this.props.filterItems(this.state.categoriesFilterList);
    }
    else{
      this.state.categoriesFilterList = [...this.state.categoriesFilterList.filter(item => item !== id)];
      
      this.props.filterItems(this.state.categoriesFilterList);
    }
     
  }
  
  //to hide toast after appearing
  hideToast = () => {
    this.setState({ ...this.state, showToast: false, toastMsg: '', toastClassName: `bg-success` });
  }
  
  //will be called when press on delete icon
  handleDelete = (event,id) =>{
    const deleteBtn = event.target;
    deleteBtn.disabled = true;
    console.log(id);
    this.props.deleteItem(id);
    console.log(store.getState());
    deleteBtn.disabled = false;
    //success delete case:
    this.setState({
      deleteModal: !this.state.deleteModal,
      showToast: true,
      toastMsg: 'Task Has Removed Successfully',
      toastClassName: `bg-success` 
    });
  }

  handleChange = event =>{
    //form elements name inside Add Task Modal
     this.setState({
       [event.target.name]: event.target.value
     });
  }
  
  //will be called when added task
  handleSubmit =  event =>{

    event.preventDefault();
    const btn = document.querySelector("#add-task-btn"); 
    btn.disabled = true;
    let id = Math.floor(Math.random() * 100) + 1;
    const obj ={
      "id": id,
      "title": event.target.title.value,
      "category": parseInt(event.target.category.value)
    };
    //console.log(obj);

    this.props.addTask(obj); 
    
    console.log(store.getState());
    btn.disabled = false;
    //success state:
    this.setState({
      addModal: !this.state.addModal,
      showToast: true,
      toastMsg: 'Task Has Added Successfully',
      toastClassName: `bg-success` 
    });

  }  

  render() {

    const {addModal, deleteModal, deleteId, showToast, toastMsg, toastClassName } = this.state;
    //from store to render them in our view
    const {categories,tasks} = this.props;

    return (
      <>
      <div className="container">
        <div className="row">
          <div className="col-xl-3 col-lg-4 col-md-5">
            <div className="sidebar-filter mt-50">
              <div className="top-filter-head">Categories Tasks</div>
              <div className="common-filter">
                {/* <div className="head">category</div> */}
                <form >
                  {categories && categories.length > 0 &&
                    <ul>
                      {categories.map((category,index) => 
                        <li className="filter-list" key={category.id} > 
                         <label >
                          <input className="pixel-radio checkboxes" type="checkbox" value={category.id} onChange={(e) => this.categoryFilter(e,category.id,index)} name="category" />
                          <span>{category.title}</span>
                         </label>
                        </li>
                      )}
                    </ul>  
                  }
                </form>
             </div>
			     </div>
			
          </div>

          <div className="col-xl-9 col-lg-8 col-md-7">
				
            <section className="lattest-product-area pb-40 category-list">
              <div className="row">
              <Button className="custom-btn btn transition-anim-comp" onClick={this.toggle}>Add Task </Button>
                <div className="col-lg-12 col-md-12" >
                  {tasks && 
                    <>
                    {tasks.map((task,index) => 
                      <div className="single-product row" key={index}>
                        <div className="product-details col-lg-12">
                          <h6 className="title-task"> {task.title} </h6>
                          <div className="price">
                            {categories.map(category => 
                              <>
                              {category.id === task.category &&
                                 <h6> {category.title} </h6>
                              }
                              </>
                            )}
                            {/* <Button className="delet-btn" onClick={() => this.toggleDeleteModal(task.id)}> */}
                               <img src={deleteIcon} className="delete-icon"  onClick={() => this.toggleDeleteModal(task.id)}/> 
                            {/* </Button> */}
                            {/* <h6> {task.category} </h6> */}
                          </div>
                        </div>
                      </div>
                    )}
                    </> 
                  }

                  {!tasks || tasks.length === 0 &&
                    <h4 className="text-center"> No Tasks For Today. </h4>
                  }
                  

                </div>

              </div>
            </section>

        </div>

    	</div>
      
    </div>
    {/* Add Task Modal */}
    <AddTaskModal
      isOpen={addModal}
      toggle={this.toggle}
      className={'modal-primary ' + this.props.className}
      handleChange={this.handleChange}
      handleSubmit={this.handleSubmit}
    />
    
    {/* Delete Task Modal */}
    <DeleteTaskModal
      isOpen={deleteModal}
      toggle={this.toggleDeleteModal}
      className={'modal-danger ' + this.props.className}
      handleDelete= {this.handleDelete}
      deleteId = {deleteId}
    />

     {
      showToast &&
      <CustomToast
        isOpen={showToast}
        msg={toastMsg}
        cb={() => this.hideToast()}
        className={toastClassName}
      />
    }
 </>
  );
  }
}

//reducers
const mapStateToProps = (state) =>{
  return{
    state: state,
    categories: state.categories,
    tasks: state.tasks
  }
}

//actions 
const mapDispatchToProps =  (dispatch) =>{
  return{
    getCategories: async () => await dispatch(getCategories()),
    getTasks:  () =>  dispatch(getTasks()),
    addTask:  obj =>  dispatch(addTask(obj)),
    deleteItem: id =>  dispatch(deleteItem(id)),
    filterItems: data => dispatch(filterItems(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);


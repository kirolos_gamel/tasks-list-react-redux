import React, { useState, useEffect, memo } from 'react';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';

export default memo(function CustomToast (props) {

      let [toast, setToast] = useState(
            {
                  msg: 'Action Succeeded',
                  className: `bg-success`,
                  isOpen: false,
                  style: { position: 'fixed', right: '1em', top: '2em', zIndex: 9999 },
                  time: 5000,
                  tout: 0
            }
      );

      let out = () => {
            if(!props.isOpen) return;
            
            setToast(
                  {
                        ...toast,
                        msg: props.msg ? props.msg : 'Action Succeeded',
                        className: props.className ? props.className : `bg-success`,
                        isOpen: props.isOpen ? props.isOpen : false,
                        style: props.style ? props.style : { position: 'fixed', right: '1em', top: '2em', zIndex: 9999 },
                        time: props.time ? props.time : 5000,
                  }
            );

            toast.tout = setTimeout(() => {
                  
                  hideToast();
                  props.cb();
                  
                  clearTimeout(toast.tout);
                  
                  //setDefaults();
                  //console.log("timeout");

            }, toast.time);
      }

      let setDefaults = () => {
            setToast(
                  {
                        msg: 'Action Succeeded',
                        className: `bg-success`,
                        isOpen: false,
                        style: { position: 'fixed', right: '1em', top: '2em', zIndex: 9999 },
                        time: 5000,
                        tout: 0
                  }
            );
      }
      
      let hideToast = () => {
            setToast(
                  {
                        ...toast,
                        isOpen: false,
                  }
            );
      }

      useEffect(() => {
            
            out();

            return () => {
                  clearTimeout(toast.tout);
                  //props.cb();
                  setDefaults();
                  //console.log("useEffect clear");
            }
      }, []);

      // let toggleToast = (state) => {
      //       setToast({ ...toast, showToast: !toast.showToast });
      // }
      
      // let hideToast = () => {
      //       setToast({ ...toast, showToast: false });
      // }

      return (
            <Toast
                  isOpen={toast.isOpen}
                  style={toast.style}
            >
                  {/* <ToastHeader toggle={toggle}>Toast title</ToastHeader> */}
                  <ToastBody className={toast.className}>
                        {toast.msg}
                  </ToastBody>
            </Toast>
      )
})



import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          loggedIn: false,
        };

    }

    logoutClick = (e) => {
        e.preventDefault();
        sessionStorage.removeItem('loginStatus');
        sessionStorage.removeItem('userInfo');
        window.location.href = "/login";
    };

    render() {
        //we want to check if we put our code inside will mount here what will happen
        return (
            <header className="header_area sticky-header">
            <div className="main_menu">
                <nav className="navbar navbar-expand-lg navbar-light main_box">
                    <div className="container">
                        <a className="navbar-brand logo_h" href="index.html">Kirolos Task</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                         aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>

                        <div className="collapse navbar-collapse offset" id="navbarSupportedContent">
                            <ul className="nav navbar-nav menu_nav ml-auto">
                                <li className="nav-item"><NavLink className="nav-link" to="/">Home</NavLink></li>
                                
                                <li className="nav-item"><NavLink className="nav-link" to="/tasks">Tasks</NavLink></li>
                                <li className="nav-item"><NavLink className="nav-link" to="/profile">Profile</NavLink></li>

                                <li className="nav-item"><a className="nav-link" href="#" onClick={(e)=>this.logoutClick(e)} >SignOut</a></li>

                            </ul>
                           
                        </div>

                    </div>
                </nav>
            </div>
        
           </header>
        );
      }
}

export default Header;
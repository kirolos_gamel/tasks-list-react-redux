import React from 'react';

const Footer = () =>{
    return(
        <footer className="footer-area section_gap">
			<div className="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
				<p className="footer-text m-0">
Copyright &copy; All rights reserved | This task is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">Kirolos Gamel</a>

</p>
			</div>

	</footer>
    );
}

export default Footer;
import React, { Component, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';


// routes config
import routes from '../routes';


const Footer = React.lazy(() => import('./Footer'));
const Header = React.lazy(() => import('./Header'));

class Layout extends Component {

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  // signOut(e) {
  //   e.preventDefault()
  //   this.props.history.push('/login')
  // } onLogout={e=>this.signOut(e)}

  render() {
    return (
      <div className="app">

        <Suspense  fallback={this.loading()}>
           <Header />
        </Suspense>

        <div className="container">
            <Suspense fallback={this.loading()}>
                <Switch>
                    {routes.map((route, idx) => {
                    return route.component ? (
                        <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                            <route.component {...props} />
                        )} />
                    ) : (null);
                    })}
                  {/* <Redirect from="/" to="/profile" /> */}
                </Switch>
            </Suspense>
        </div>

        <Suspense  fallback={this.loading()}>
           <Footer />
        </Suspense>
      </div>
    );
  }
}

export default Layout;

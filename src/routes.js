import React from 'react';

const Home = React.lazy(() => import('./views/Home'));
const Profile = React.lazy(() => import('./views/Profile/Profile'));
const Tasks = React.lazy(() => import('./views/Tasks/Tasks'));

const routes = [
    { path: '/', exact: true, name: 'Home', component: Home },
    { path: '/profile', name: 'Profile', component: Profile },
    { path: '/tasks', name: 'Tasks', component: Tasks },
];
  
export default routes;